module tools

go 1.15

require (
	github.com/cheekybits/is v0.0.0-20150225183255-68e9c0620927 // indirect
	github.com/dustin/go-humanize v1.0.0 // indirect
	github.com/envoyproxy/protoc-gen-validate v0.1.0
	github.com/favadi/protoc-go-inject-tag v1.1.0
	github.com/fsnotify/fsnotify v1.4.9 // indirect
	github.com/grpc-ecosystem/grpc-gateway/v2 v2.2.0
	github.com/iancoleman/strcase v0.1.3 // indirect
	github.com/lyft/protoc-gen-star v0.5.2 // indirect
	github.com/matryer/try v0.0.0-20161228173917-9ac251b645a2 // indirect
	github.com/spf13/pflag v1.0.5 // indirect
	github.com/stripe/safesql v0.2.0
	github.com/tdewolff/minify v2.3.6+incompatible
	github.com/tdewolff/parse v2.3.4+incompatible // indirect
	github.com/tdewolff/test v1.0.6 // indirect
	gitlab.com/ovsinc/protoc-gen-web-handler v1.3.2
	google.golang.org/grpc/cmd/protoc-gen-go-grpc v1.1.0
	google.golang.org/protobuf v1.25.0
)
