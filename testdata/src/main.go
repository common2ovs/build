package main

import "fmt"

func echo(s string) {
	fmt.Println(s)
}

func main() {
	echo("Hello world")
}
