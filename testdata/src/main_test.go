package main

import "testing"

func Test_echo(t *testing.T) {
	tests := []struct {
		name string
	}{
		{name: "hello"},
	}
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			echo(tt.name)
		})
	}
}
