#!/usr/bin/env bash

version="$(cat Dockerfile | head -n 1 | grep FROM | awk -F: '{print $2}')"
hub_image="pospisku/gobuild"
gitlab_reg="registry.gitlab.com"
gitlab_image="$gitlab_reg/pospiskurf/pospiskud/build"


if [[ $1 == "build" ]]; then

  echo "=== BUILD ==="
  docker build -t \
    	$hub_image:$version . || exit 1
  echo " --- DONE --- "

  docker tag $hub_image:$version $hub_image:latest

  exit 0
fi


if [[ $1 == "push" ]]; then

	echo "=== PUSH to hub ==="

	docker login || exit 2

	docker tag \
		$hub_image:$version \
		$hub_image:latest

	docker push \
		$hub_image:$version
	docker push \
		$hub_image:latest

	docker logout

	echo "--- DONE ---"


	echo "=== PUSH to gitlab ==="

	docker login $gitlab_reg || exit 3

	docker tag \
		$hub_image:$version \
		$gitlab_image:$version

	docker push \
		$gitlab_image:$version

	docker tag \
		$hub_image:$version \
		$gitlab_image:latest

	docker push \
		$gitlab_image:latest

	docker logout $gitlab_reg

	echo "--- DONE ---"
  
	exit 0
fi

if [[ $1 == "test" ]]; then
  pushd ./testdata &>/dev/null
  docker build --force-rm --no-cache .
  popd &>/dev/null

  exit 0
fi

echo "Use build|push|test commannds"
exit 255

