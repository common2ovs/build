# Build repo pospisku/gobuild

## Состав

Пакет | Версия
----- | ------
Go | 1.15.8
Clang | 10
Clang++ | 10
minify | dev
safesql | dev
golangci-lint | 1.37.0
protoc | 3.15.1
protoc-go-inject-tag | dev
protoc-gen-go | dev
protoc-gen-go-grpc | dev
protoc-gen-validate | dev
protoc-gen-grpc-gateway | v2
protoc-gen-openapiv2 | v2
protoc-gen-web-handler | 1.1
make | 4.2.1
gcc | 8.3.0

## Сборка

./build.build

## Тестирование

./build.sh test
