FROM golang:1.15.10-buster

LABEL maintainer="Alex OvsInc <ovsinc at ya dot ru>"

RUN echo "Go docker image version: $GOLANG_VERSION"

ENV GOPATH /go
ENV GOBIN /usr/local/bin
ENV GOROOT /usr/local/go

ENV PATH $PATH:${GOROOT}/bin:/usr/local/bin
ENV PROTOBUF_VER 3.15.6
ENV GRPC_WEB 1.2.1
ENV GO_LINT 1.39.0
ENV PROTOTOOL_VER 1.10.0
ENV CLANG_VER 11

# Add apt key for LLVM repository
RUN wget -q -O - https://apt.llvm.org/llvm-snapshot.gpg.key | apt-key add -

# Add LLVM apt repository
RUN echo "deb http://apt.llvm.org/buster/ llvm-toolchain-buster-${CLANG_VER} main" | tee -a /etc/apt/sources.list

RUN apt-get update -y && apt-get upgrade -y && apt-get autoremove -y

# Install clang from LLVM repository
RUN apt-get install -y --no-install-recommends \
    clang-${CLANG_VER} \
    unzip \
    bash \
    patch
RUN apt-get clean && \
    rm -rf /var/lib/apt/lists/* /tmp/* /var/tmp/*


RUN curl -sSL0 \
    -o /tmp/protoc.zip \
    https://github.com/protocolbuffers/protobuf/releases/download/v${PROTOBUF_VER}/protoc-${PROTOBUF_VER}-linux-x86_64.zip \
    && \
    unzip -od /usr/local /tmp/protoc.zip && \
    rm -f /tmp/protoc.zip

# Set Clang as default CC
ENV set_clang /etc/profile.d/set-clang-cc.sh
RUN ln -s /usr/bin/clang-${CLANG_VER} /usr/bin/clang && \
    ln -s /usr/bin/clang++-${CLANG_VER} /usr/bin/clang++
RUN echo "export CC=clang" | tee ${set_clang}
RUN echo "export CXX=clang++" | tee -a ${set_clang}
RUN echo >> ${set_clang}
RUN chmod a+x ${set_clang}

ADD ./tools /tools

RUN cd /tools && go mod tidy

# GO
RUN cd /tools && \
    go install github.com/stripe/safesql \
    github.com/tdewolff/minify/cmd/minify

# PROTOBUF
RUN cd /tools && \
    go install \
    google.golang.org/protobuf/cmd/protoc-gen-go \
    google.golang.org/grpc/cmd/protoc-gen-go-grpc \
    github.com/envoyproxy/protoc-gen-validate \
    gitlab.com/ovsinc/protoc-gen-web-handler \
    github.com/favadi/protoc-go-inject-tag \
    github.com/grpc-ecosystem/grpc-gateway/v2/protoc-gen-openapiv2 \
    github.com/grpc-ecosystem/grpc-gateway/v2/protoc-gen-grpc-gateway

RUN curl -sSL0 \
    -o ${GOBIN}/prototool \
    https://github.com/uber/prototool/releases/download/v${PROTOTOOL_VER}/prototool-Linux-x86_64 && \
    chmod +x ${GOBIN}/prototool


RUN curl -sSL0 \
    -o ${GOBIN}/protoc-gen-grpc-web \
    https://github.com/grpc/grpc-web/releases/download/${GRPC_WEB}/protoc-gen-grpc-web-${GRPC_WEB}-linux-x86_64 && \
    chmod +x ${GOBIN}/protoc-gen-grpc-web

RUN curl -sSL0 \
    https://github.com/golangci/golangci-lint/releases/download/v${GO_LINT}/golangci-lint-${GO_LINT}-linux-amd64.tar.gz | \
    tar -C /tmp -xzf - && \
    mv /tmp/golangci-lint-${GO_LINT}-linux-amd64/golangci-lint \
    ${GOBIN}/golangci-lint && \
    rm -rf /tmp/golangci-lint-${GO_LINT}-linux-amd64/golangci-lint

RUN mkdir /build

VOLUME /build
